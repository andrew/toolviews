Toolviews
=========

Collect and report analytic data on Toolforge usage.

Data collection
---------------

Data collection is done using the load_tools_stats.py script. This script
expects to be given one or more Nginx access log files to parse. The log files
are expected to be using the "vhost" logging format used by Wikimedia's
dynamicproxy reverse proxy service:

```
'$host $remote_addr - $remote_user [$time_local] "$request" $status $body_bytes_sent "$http_referer" "$http_user_agent"'
```

See config.yaml.example for an example configuration file that can be used
with load_tools_stats.py after setting the correct values for your local
environment.

License
-------
[GPL-3.0-or-later](https://www.gnu.org/copyleft/gpl.html "GNU GPL 3.0 or later")
