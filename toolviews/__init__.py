# -*- coding: utf-8 -*-
#
# This file is part of Toolviews
#
# Copyright (C) 2019 Wikimedia Foundation and contributors
# All Rights Reserved.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import pymysql.cursors
import toolforge


from . import cache


def dbconn():
    return toolforge.connect(
        dbname='s53734__toolviews_p',
        cluster='labsdb',
        host='tools.db.svc.eqiad.wmflabs',
    )


def fetch_all(sql, params=None):
    """Execute an SQL query and return all rows."""
    conn = dbconn()
    try:
        with conn.cursor(pymysql.cursors.DictCursor) as cur:
            cur.execute(sql, params)
            return cur.fetchall()
    finally:
        conn.close()


def hits(date, tool='*'):
    date_str = date.strftime('%Y-%m-%d')
    key = "hits:{}:{}".format(tool, date_str)
    data = cache.CACHE.load(key)
    if data is None:
        sql = (
            "SELECT hits, tool "
            "FROM daily_raw_views "
            "WHERE request_day = %s "
        )
        params = (date_str,)
        if tool != '*':
            sql += "AND tool = %s "
            params += (tool,)
        sql += "ORDER BY hits desc"
        data = fetch_all(sql, params)
        cache.CACHE.save(key, data, 3600)
    return data


def hits_range(start, end, tool='*'):
    start_str = start.strftime('%Y-%m-%d')
    end_str = end.strftime('%Y-%m-%d')
    key = "hits:{}:{}:{}".format(tool, start_str, end_str)
    data = cache.CACHE.load(key)
    if data is None:
        sql = (
            "SELECT request_day, tool, hits "
            "FROM daily_raw_views "
            "WHERE request_day >= %s "
            "AND request_day <= %s "
        )
        params = (start_str, end_str,)
        if tool != '*':
            sql += "AND tool = %s "
            params += (tool,)
        sql += "ORDER BY request_day desc, hits desc"
        # Convert datatime.datetime data from the db to ISO8601 strings
        data = [
            {
                'date': row['request_day'].strftime('%Y-%m-%d'),
                'tool': row['tool'],
                'hits': row['hits'],
            }
            for row in fetch_all(sql, params)
        ]
        cache.CACHE.save(key, data, 3600)
    return data


def list_tools():
    key = "tools:list"
    data = cache.CACHE.load(key)
    if data is None:
        sql = (
            "SELECT DISTINCT(tool) "
            "FROM daily_raw_views "
            "ORDER BY tool "
        )
        data = fetch_all(sql)
        cache.CACHE.save(key, data, 3600)
    return data
