# -*- coding: utf-8 -*-
#
# This file is part of Toolviews
#
# Copyright (C) 2018 Wikimedia Foundation and contributors
# All Rights Reserved.
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import collections
import logging.config
import os

import flasgger
import flask
import flask_cors
import werkzeug.middleware.proxy_fix
import yaml

import toolviews
import toolviews.routing


logging.config.dictConfig({
    'version': 1,
    'formatters': {
        'default': {
            'format': '%(asctime)s %(name)-12s %(levelname)-8s: %(message)s',
            'datefmt': '%Y-%m-%dT%H:%M:%SZ',
        },
    },
    'handlers': {
        'wsgi': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stderr',
            'formatter': 'default',
        },
    },
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    },
})


# Create the Flask application
app = flask.Flask(__name__)
# Add the ProxyFix middleware which reads X-Forwarded-* headers
app.wsgi_app = werkzeug.middleware.proxy_fix.ProxyFix(app.wsgi_app)

# Load configuration from YAML file(s).
__dir__ = os.path.dirname(__file__)
app.config.update(
    yaml.safe_load(open(os.path.join(__dir__, 'default_config.yaml'))))
try:
    app.config.update(
        yaml.safe_load(open(os.path.join(__dir__, 'config.yaml'))))
except IOError:
    # It is ok if there is no local config file
    pass

# Register custom route converters
app.url_map.converters['date'] = toolviews.routing.DateConverter
# Decorate API responses with CORS headers
cors = flask_cors.CORS(app, resources={r'/api/*': {'origins': '*'}})

# Setup an OpenAPI 3.0 spec for API endpoints
oas3 = flasgger.Swagger(app, template_file='oas3_api.yaml')


@app.route('/')
def index():
    """Application landing page."""
    return flask.render_template('index.html')


@app.route('/api/v1/day/<date:day>')
@app.route('/api/v1/tool/<string:tool>/day/<date:day>')
def get_hits_for_date(day, tool='*'):
    """Get hits for a tool (or all tools) on a given date."""
    return flask.jsonify({
        'date': day.strftime('%Y-%m-%d'),
        'tool': tool,
        'results': {
            row['tool']: row['hits']
            for row in toolviews.hits(day, tool)
        },
    })


@app.route('/api/v1/daily/<date:start>/<date:end>')
@app.route('/api/v1/tool/<string:tool>/daily/<date:start>/<date:end>')
def get_hits_for_daterange(start, end, tool='*'):
    """Get hits for a tool (or all tools) during a date range."""
    hits = collections.defaultdict(dict)
    for row in toolviews.hits_range(start, end, tool):
        hits[row['date']][row['tool']] = row['hits']
    return flask.jsonify({
        'start': start.strftime('%Y-%m-%d'),
        'end': end.strftime('%Y-%m-%d'),
        'tool': tool,
        'results': hits,
    })


@app.route('/api/v1/tools')
def get_tools():
    return flask.jsonify({
        'results': [row['tool'] for row in toolviews.list_tools()],
    })
